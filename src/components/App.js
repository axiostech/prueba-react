import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

//components
import Login from "./Login";
import Home from "./Home";
import PageNotFound from "./PageNotFound";

//css
import "../css/App.css";

//services
import AuthService from "../services/auth.service";

function App() {
  return (
    <Router>
      <Switch>
        <Route component={Login} exact path="/"></Route>
        <Route path="/home">
          {
            AuthService.getAuth() ? <Home/> : <Redirect to="/"></Redirect>
          }
        </Route>
        <Route component={PageNotFound}></Route>
      </Switch>
    </Router>
  );
}

export default App;
