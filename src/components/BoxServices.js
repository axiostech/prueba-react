//css
import "../css/BoxServices.css";

//components
import ImgService from './ImgService';

function BoxServices() {
  return (
    <div className="contBox d-flex justify-content-center">
      <ImgService img="./img/s1.svg" text="Dog Walking"/>
      <ImgService img="./img/s2.svg" text="Pet Sitting"/>
      <ImgService img="./img/s3.svg" text="Overnight Care"/>
      <ImgService img="./img/s4.svg" text="Other Services"/>
    </div>
  );
}

export default BoxServices;
