//css
import "../css/BoxServices.css";

//components
import ImgService from "./ImgService";

function BoxServices2() {
  return (
    <div className="trazado">
      <div className="contBox d-flex justify-content-start">
        <ImgService img="./img/s5.svg" text="Dog Walking"/>
      </div>
      <div className="contBox d-flex justify-content-end ml-5">
        <ImgService img="./img/s6.svg" text="Pet Sitting" />
      </div>
      <div className="contBox d-flex justify-content-start">
        <ImgService img="./img/s7.svg" text="Overnight Care" />
      </div>
    </div>
  );
}

export default BoxServices2;
