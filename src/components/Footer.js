import { Container, Row, Col } from "react-bootstrap";

//css
import '../css/Footer.css';

function Footer() {
  return (
    <Container className="pt-5 contFooter">
      <Row>
        <Col>
          <ul>
            About
            <li>Locations</li>
            <li>Franchise With Us</li>
            <li>Partners</li>
            <li>About Us</li>
            <li>Make Fetch Happen!</li>
          </ul>
        </Col>
        <Col>
          <ul>
            Resources
            <li>Reviews</li>
            <li>Pet Resource Center</li>
            <li>Media Fact Sheet</li>
            <li>Blog</li>
            <li>News</li>
          </ul>
        </Col>
        <Col className="d-flex flex-columns align-items-end">
          <ul>
            <li>Gift Cards</li>
            <li>Services</li>
            <li>Franchisee Login</li>
            <li>Terms of Use</li>
            <li>Privacy Policy</li>
          </ul>
        </Col>
        <Col>
            <div className="newsletterCont">
                <h6>NewsLetter</h6>
                <p>Sign up to receive the Fetch! Family Newsletter</p>
                <div>
                    <input type="email" placeholder="Email Adress"/>
                    <button><i className="fas fa-long-arrow-alt-right"></i></button>
                </div>
            </div>
        </Col>
      </Row>
    </Container>
  );
}

export default Footer;
