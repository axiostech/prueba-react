import { Container, Row, Col, Button, Image } from "react-bootstrap";

//components
import NavBar from "./NavBar";
import OurServices from "./OurServices";
import Swiper from "./Swiper";
import BoxServices from "./BoxServices";
import BoxServices2 from "./BoxServices2";
import MakeFetch from './MakeFetch';
import Footer from './Footer';

//css
import "../css/Home.css";

//assets
import perro from "../img/dog.png";

function Home() {
  return (
    <div className="w-100 h-100 bgMain">
      <NavBar />
      <Container className="h-75 d-flex justify-content-center align-items-center py-5">
        <Row className="d-flex align-items-center">
          <Col className="col-12 col-sm-12 col-md-6 col-lg-6">
            <div className="contWeGetPet">
              <h1>We Get Pet Care!</h1>
              <p className="w-75">
                For over 17 Years, Fetch! Pet Care has been a trusted partner in
                keeping pets healthy and happy!
              </p>
              <div>
                <Button className="py-3 px-4">Schedule Service</Button>
                <span>Or Call 866.338.2463</span>
              </div>
            </div>
          </Col>
          <Col className="col-12 col-sm-12 col-md-6 col-lg-6 d-flex align-items-center justify-content-center">
            <div className="contImgPerro d-flex align-items-center justify-content-center">
              <Image src={perro} alt="imagen de perrito" className="w-100" />
            </div>
          </Col>
        </Row>
      </Container>
      <br />
      <br />
      <div className="w-100 h-75 mt-5 bgServices">
        <Container className="h-75 d-flex justify-content-start align-items-start py-5">
          <Row className="d-flex align-items-start">
            <Col className="col-12 col-sm-12 col-md-6 col-lg-6">
              <OurServices
                title="Our Services"
                paragraph="National Brand With a Local Feel. Experience the Fetch! Difference"
              />
            </Col>
            <Col className="col-12 col-sm-12 col-md-6 col-lg-6 d-flex align-items-start justify-content-end">
              <BoxServices />
            </Col>
          </Row>
        </Container>
      </div>
      <div className="w-100">
        <Swiper />
      </div>
      <div className="w-100 h-75 mt-5 bgServices">
        <Container className="h-75 d-flex justify-content-start align-items-start py-5">
          <Row className="d-flex align-items-start">
            <Col className="col-12 col-sm-12 col-md-6 col-lg-6">
              <BoxServices2 />
            </Col>
            <Col className="col-12 col-sm-12 col-md-6 col-lg-6 d-flex align-items-end justify-content-start">
              <OurServices
                title="How it Works"
                paragraph="Because finding a good pet sitter shouldn’t be a hassle. With Fetch! It’s as easy as…"
              />
            </Col>
          </Row>
        </Container>
      </div>
      <div className="w-100">
        <MakeFetch/>
      </div>
      <div className="w-100 mt-5 bgFooter py-4">
        <Footer />
      </div>
    </div>
  );
}

export default Home;
