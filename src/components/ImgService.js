//css
import { Image } from "react-bootstrap";
import "../css/BoxServices.css";

function BoxServices(props) {
  return (
    <div className="d-flex justify-content-center align-items-center flex-columns">
      <Image src={props.img} alt="imagen de caja de servicios" />
      <p className="m-0 mt-3">{props.text}</p>
    </div>
  );
}

export default BoxServices;
