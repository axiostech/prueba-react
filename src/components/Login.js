import { useState } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button, Form, Image } from "react-bootstrap";

//alerts
import Swal from "sweetalert2";

// assets
import perro from "../img/pet.svg";

//css
import "../css/Login.css";

// services
import AuthService from "../services/auth.service";

function Login(props) {
  const [loadVisible, setLoadVisible] = useState(false);
  const [loginDisable, setDisableLogin] = useState(true);
  const [confirmSubmit, setConfirmSubmit] = useState({
    correo: false,
    pass: false,
  });
  const [user, setUser] = useState({
    correo: "",
    correoError: false,
  });
  const [pass, setPass] = useState({
    password: "",
    passwordError: false,
  });

  const handleUser = (event) => {
    const value = event.target.value;
    if (/.+@.+\.[A-Za-z]+$/.test(value)) {
      event.target.classList.add("is-valid");
      event.target.classList.remove("is-invalid");
      setUser({
        correo: value,
        correoError: false,
      });
      setConfirmSubmit({
        correo: true,
        pass: confirmSubmit.pass,
      });
    } else {
      event.target.classList.remove("is-valid");
      event.target.classList.add("is-invalid");
      setUser({
        correo: value,
        correoError: true,
      });
      setConfirmSubmit({
        correo: false,
        pass: confirmSubmit.pass,
      });
    }
    viewError();
  };

  const handlePass = (event) => {
    const value = event.target.value;
    if (value.length >= 6) {
      event.target.classList.add("is-valid");
      event.target.classList.remove("is-invalid");
      setPass({
        password: value,
        passwordError: false,
      });
      setConfirmSubmit({
        correo: confirmSubmit.correo,
        pass: true,
      });
    } else {
      event.target.classList.remove("is-valid");
      event.target.classList.add("is-invalid");
      setPass({
        password: value,
        passwordError: true,
      });
      setConfirmSubmit({
        correo: confirmSubmit.correo,
        pass: false,
      });
    }
    viewError();
  };

  const viewError = () => {
    if (confirmSubmit.correo && confirmSubmit.pass) {
      setDisableLogin(false);
    } else {
      setDisableLogin(true);
    }
  };

  const handleSubmit = (event) => {
    setLoadVisible(true);
    event.preventDefault();
    AuthService.login(user.correo, pass.password)
      .then((res) => {
        AuthService.setToken(res["data"].token);
        setLoadVisible(false);
        window.location = "/home";
      })
      .catch((err) => {
        console.log(err);
        Swal.fire({
          icon: "error",
          title: "Oops...",
          backdrop: false,
          text: "Error iniciando sesión",
          footer: "Intenta de nuevo!",
        });
        setLoadVisible(false);
      });
  };

  return (
    <div className="w-100 h-100 bgLogin d-flex justify-content-center align-items-center">
      <Container>
        <Row className="row d-flex align-items-center">
          <Col className="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div className="perro bg-white p-5 d-flex justify-content-center align-items-center rounded-circle">
              <Image src={perro} alt="logo de prueba tecnica" width="90%" />
            </div>
          </Col>
          <Col className="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <Form onSubmit={handleSubmit}>
              <h1 className="tituloLogin">Bienvenido</h1>
              <br />
              <input
                type="email"
                placeholder="EMAIL"
                className="mt-3"
                name="correo"
                value={user.correo}
                onChange={handleUser}
                required
              />
              {user.correoError ? (
                <div className="mt-1">
                  <span className="text-danger">Correo Invalido</span>
                </div>
              ) : null}
              <input
                type="password"
                placeholder="CONTRASEÑA"
                className="mt-3"
                name="password"
                value={pass.password}
                onChange={handlePass}
                required
              />
              {pass.passwordError ? (
                <div className="mt-1">
                  <span className="text-danger">Mínimo 6 Caracteres</span>
                </div>
              ) : null}
              <div className="buttonOlvidaste mt-4 d-flex justify-content-end">
                <Link to="/login">¿Olvidaste tu contraseña?</Link>
              </div>
              <div className="contBtnLogin d-flex justify-content-center mt-5">
                <Button
                  disabled={loginDisable}
                  type="submit"
                  className="btnLogin py-2 px-4">
                  Iniciar Sesión
                  {loadVisible ? (
                    <i className="fas fa-spinner fa-spin ml-3"></i>
                  ) : null}
                </Button>
              </div>
              <div className="mt-4 d-flex contRegistrarse d-flex justify-content-center">
                <p className="m-0">AUN NO TIENES CUENTA</p>
                <Link to="/login">REGISTRARSE</Link>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Login;
