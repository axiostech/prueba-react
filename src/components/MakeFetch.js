import { Col, Container, Row, Image } from "react-bootstrap";

//assets
import perro from "../img/dog2.png";

//css 
import '../css/MakeFetch.css';

function MakeFetch() {
  return (
    <Container className="contMake p-5">
      <Row>
        <Col>
          <div className="w-100 d-flex">
            <div className="contImgPerroMake">
              <Image src={perro} alt="chiguagua logo" />
            </div>
            <div className="contTextMake">
              <h1>Make Fetch! Happen</h1>
              <p className="mt-4">
                If you love pets and want exciting work, apply to be a Fetch!
                Pet Care Provider! We train every team member and provide
                ongoing support to help you get the most from your experience.
              </p>
              <button className="mt-4">Join Now</button>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default MakeFetch;
