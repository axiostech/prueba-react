import { Navbar, Nav, Container, Image } from "react-bootstrap";
import { Link } from "react-router-dom";

//css
import "../css/NavBar.css";

// assets
import perro from "../img/pet.svg";

function NavBar() {
  return (
    <Navbar className="pt-3" expand="lg">
      <Container>
        <Link to="/home">
          <div className="contPerro bg-white rounded-circle p-2 d-flex justify-content-center align-items-center">
            <Image src={perro} alt="logo prueba técnica react" />
          </div>
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Link to="/home">Location</Link>
            <Link to="/home">Blog</Link>
            <Link to="/home">Services</Link>
            <Link to="/home">About Us</Link>
            <Link to="/home">Franchise with Us</Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;
