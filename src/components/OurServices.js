//css
import '../css/OurServices.css';

function OurServices(props) {
  return (
    <div className="OurServices w-100 ml-5">
      <h1>{props.title}</h1>
      <p>{props.paragraph}</p>
      <h6 className="mb-3 mt-4">Enter Your Location and Fetch Our Services</h6>
      <div className="d-flex justify-content-between align-items-center p-2">
          <input type="text" placeholder="Zip Code"/>
          <button><i className="fas fa-long-arrow-alt-right"></i></button>
      </div>
    </div>
  );
}

export default OurServices;
