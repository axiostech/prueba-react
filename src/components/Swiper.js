import { Col, Container, Row, Image } from "react-bootstrap";
import { Carousel } from "3d-react-carousal";

//css
import "../css/Swiper.css";

//assets
import perfil from "../img/perfil.jpg";

function Swiper() {
  const slides = [
    <div className="carrouselCont">
      <div>
        <Image src={perfil} alt="recomendacion" className="w-100" />
      </div>
      <span>Daniel Mateus</span>
      <p>
        " I like that I got to meet the dog Walker who is walking my dog daily
        and consult with her. I also appreciate the daily communication I get
        about the dog and how my dog is doing"
      </p>
    </div>,
    <div className="carrouselCont">
      <div>
        <Image src={perfil} alt="recomendacion" className="w-100" />
      </div>
      <span>Daniel Mateus</span>
      <p>
        " I like that I got to meet the dog Walker who is walking my dog daily
        and consult with her. I also appreciate the daily communication I get
        about the dog and how my dog is doing"
      </p>
    </div>,
    <div className="carrouselCont">
      <div>
        <Image src={perfil} alt="recomendacion" className="w-100" />
      </div>
      <span>Daniel Mateus</span>
      <p>
        " I like that I got to meet the dog Walker who is walking my dog daily
        and consult with her. I also appreciate the daily communication I get
        about the dog and how my dog is doing"
      </p>
    </div>,
  ];
  //
  return (
    <Container>
      <Row>
        <Col>
          <h1 className="titleSwiper">
            Here's what pet owners have to say about Fetch! Pet Care...
          </h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <Carousel slides={slides} autoplay={true} interval={10000000} />
        </Col>
      </Row>
    </Container>
  );
}

export default Swiper;
