import React from 'react';
import ReactDOM from 'react-dom';

//app
import App from './components/App';

//css index
import './css/index.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
