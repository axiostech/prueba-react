import axios from "axios";

const AuthService = {
  login: (email, password) => {
    const request = {
      email,
      password,
    };
    return axios.post("https://reqres.in/api/login", request);
  },
  setToken: (token) => {
      localStorage.setItem('TokenReact', token);
  },
  getAuth: () => {
    if(localStorage.getItem('TokenReact')){
      return true;
    } else {
      return false;
    }
  }
};

export default AuthService;
